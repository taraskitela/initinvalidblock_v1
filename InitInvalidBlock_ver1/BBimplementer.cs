﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAND_Prog;
using System.ComponentModel.Composition;


namespace InitInvalidBlock_v1

{

    //Ця DLL експортує реалізатора бед-блок функціоналу


    [Export(typeof(ChipExtension)),
        ExportMetadata("Name", "InitInvalidBlock_v1")]
    public class BBimplementer : ChipExtension  , IBBImplementer
    {
        // InitInvalidBlock_ver1 - це для K9F1G08U0D  і йому подібних

        private byte _good_mark = 0xFF;               // признак валідних даних

        private Page page1;
        private Page page2;
        private bool page1_state;
        private bool page2_state;
       

        #region IBBImplementer Support

        public void CheckPages()
        {
            Chip.instance.UnCheck();
            foreach (LUN lun in Chip.instance.GetSubPart())                                //В кожному LUN-і
            {
                foreach (Block block in lun.MemoryArea(0).GetSubPart()) // В кожному блоці 
                {
                    block.Page(0).Check();                          // шукаю 1 і 2 сторінки (індекси 0 і 1) - і помічаю їх для читання
                    block.Page(1).Check();                          // і помічаю їх для читання

                }
            }
        }

        public bbResult BlockIsBad(Page page, byte[] data)
        {


            if (page.id == 0)
            {
                page1 = page;
                page1_state = DataAnalise(data);
            }
            else
            if (page.id == 1)
            {
                page2 = page;
                page2_state = DataAnalise(data);
            }
            else
                return bbResult.blockNoDefine;


            try
            {
                if (page1.parent == page2.parent)        //Тобто маю інфу з обох сторінок одного блока ,  можу приймати рішення про блок
                {
                    if (page1_state && page2_state)
                        return bbResult.blockIsOk;
                    else
                        return bbResult.blockIsBad;
                }
                else
                    return bbResult.blockNoDefine;

            }
            catch
            {
                return bbResult.blockNoDefine;
            }



        }

        #endregion

        private bool DataAnalise(byte[] data)
        {
            if (data[Chip.memOrg.bytesPP] != _good_mark)   // якщо в даних по індексу 2048(початок SpareArea) не 0xFF то це bad-blok
                return false;
            else
                return true;

        }
    }
}

